library(neuralnet)
# először is fel kell telepítenünk a neuralnet könyvtárat a program használatához, amit a következőképpen tudunk megtenni: R a terminalba, majd install.packages('neuralnet')
# majd a programot az Rscript nr.r el tudjuk futtatni
# ahogy lehet látni, az eredményekből kirajzolja a hálót
a1    <- c(0,1,0,1)
a2    <- c(0,0,1,1)

# Az a1 és a2 ből megkapott OR értékek 
# 0 or 0 = 0 | 1 or 0 = 1 | 0 or 1 = 1 | 1 or 1 = 1
OR    <- c(0,1,1,1) 

or.data <- data.frame(a1, a2, OR)

nn.or <- neuralnet(OR~a1+a2, or.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

# Kirajzoljuk az OR neurális hálót
plot(nn.or) # ezeket a plotokat menti el, a terminalba szintén láthatóak részletek

compute(nn.or, or.data[,1:2])

a1    <- c(0,1,0,1)
a2    <- c(0,0,1,1)

# Az a1 és a2 ből megkapott OR értékek 
# 0 or 0 = 0 | 1 or 0 = 1 | 0 or 1 = 1 | 1 or 1 = 1
OR    <- c(0,1,1,1)

# És a megkapott AND érték
AND   <- c(0,0,0,1)

orand.data <- data.frame(a1, a2, OR, AND)

nn.orand <- neuralnet(OR+AND~a1+a2, orand.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

# Kirajzoljuk az ORAND neurális hálót
plot(nn.orand)

compute(nn.orand, orand.data[,1:2])

a1      <- c(0,1,0,1)
a2      <- c(0,0,1,1)

# A megkapott EXOR érték
EXOR    <- c(0,1,1,0)

exor.data <- data.frame(a1, a2, EXOR)

nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=0, linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

# Kirajzoljuk az EXOR neurális hálót
plot(nn.exor)

compute(nn.exor, exor.data[,1:2])

a1      <- c(0,1,0,1)
a2      <- c(0,0,1,1)

# A megkapott EXOR érték
EXOR    <- c(0,1,1,0)

exor.data <- data.frame(a1, a2, EXOR)

# hidden=c(6, 4, 6), 6, 4, illetve ismét 6 rejtett neuron 
# itt már újabb neuronokkal kellett bővítenünk a neuralnetet, hogy megfelelő értéket kapjunk
# a neuronok egy egész számok , vektorban találhatóak 
nn.exor <- neuralnet(EXOR~a1+a2, exor.data, hidden=c(6, 4, 6), linear.output=FALSE, stepmax = 1e+07, threshold = 0.000001)

# Kirajzoljuk az EXOR neurális hálót
plot(nn.exor)

compute(nn.exor, exor.data[,1:2])

