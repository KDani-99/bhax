public class ExorTitkosító {
                
    /* A program használatakor az alábbi argumentumokat vesszük be inputként (a konstruktornak): 

    Egy kulcsot
    Egy input streamet
    Egy output streamet

    */

    És a program IOException (hibát)-t dobhat, ha van valami probléma (esetleg nincs file)

    public ExorTitkosító(String kulcsSzöveg, 
            java.io.InputStream bejövőCsatorna,
            java.io.OutputStream kimenőCsatorna)
            throws java.io.IOException {
        
        /*
        
        A kulcsot eltároljuk egy byte tömbbe (az argumentumként átadott kulcsot átalakítjuk)
        Majd egy 256 méretü buffert-t hozunk létre ahol eltároljuk az átadott, titkosítani kívánt szöveget

        */

        byte [] kulcs = kulcsSzöveg.getBytes(); 
        byte [] buffer = new byte[256];

        /*

        A kulcsIndex-ben tároljuk, hogy hol járunk
        Az olvasottBájtokban tároljuk, hogy hány byte-t olvastunk be

        */

        int kulcsIndex = 0;
        int olvasottBájtok = 0;

        /*

        Beolvassuk a buffer byte tömbbe az összes titkosítani kívánt szöveget

        */

        while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) {
            
            for(int i=0; i<olvasottBájtok; ++i) {

                // Majd itt titkosítjuk a kulccsal a szövegünket (xorral)

                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex+1) % kulcs.length;
                
            }
            
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);
            
        }
        
    }
    
    /*

    A program main() függvényében hívjuk meg a titkosító osztályunkat

    */

    public static void main(String[] args) {
        
        try {
            
            new ExorTitkosító(args[0], System.in, System.out); // Létrehozunk egy új ExorTitkosító objektumot, melynek átadjuk a megfelelő paramétereket (kulcs az első argumentum,inputStream,outputStream)
            
            // Mivel az osztály konstruktorában titkosítunk, ezért nem kell semmilyen függvényét meghívnunk (nincs is)

        } catch(java.io.IOException e) {

            // Illetve elkapjuk, ha hibát dob és kiíratjuk
            
            e.printStackTrace();
            
        }
        
    }
    
}