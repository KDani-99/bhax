public class LeetCipher
{
    /* Készítünk egy statikus Map-et, amiben eltároljuk a KV párokat

        A kulcs egy karakter lesz, amivel elérhetjük az adott karakterhez tartozó (hasonló) karakterek tömbjét

        A lista privát, mivel csak osztályon belül fogjuk használni, illetve lehet final is, mivel nem tervezünk új Mapot eltárolni

    */
    private static final Map<Character,String []> characters = new HashMap<Character,String[]>()
    {
        {
            put('a',new String[]{
                    "4",
                    "/-\\",
                    "@",
                    "a",
                    "/\\"
            });
            put('b',new String[]{
                    "8","|3", "13", "|}", "|:", "|8", "18", "6", "|B", "|8"," lo", "|o", "j3", "ß", "в", "ь"
            });
            put('c',new String[]{
                    "<","{","[","(","c","c","?"
            });
            put('d',new String[]{
                    "|)","|}","|]","|>",""
            });
            put('e',new String[]{
                    "3","L","L","?","?"
            });
            put('f',new String[]{
                    "|=","ph","|#","f"
            });
            put('g',new String[]{
                    "[","-","[+","6","C-",""
            });
            put('h',new String[]{
                    "#","4","|-|","[-]","{-}","}-{","}{","|=|","[=]","{=}","/-/","(-)",")-(","I+I","?"
            });
            put('i',new String[]{
                    "1","|","!","9"
            });
            put('j',new String[]{
                    "_|","_/","_7","_)","_]","_}"
            });
            put('k',new String[]{
                    "|<","1<","l<","|{","l{"
            });
            put('l',new String[]{
                    "|_","|","1","]["
            });
            put('m',new String[]{
                    "44","|\\/|","^^","/\\/\\","/X\\","[]\\/][","[] V[]","][\\//][","(V)","//.",".\\","N\\","м"
            });
            put('n',new String[]{
                    "|\\|","/\\/","/V","]","[\\][","И","и","п"
            });
            put('o',new String[]{
                    "0","()","[]","{}","<>","Ø","oh","Θ","о","ө"
            });
            put('p',new String[]{
                    "|o","|O","|>","|*","|°","|D","/o","[] D","|7","р"
            });
            put('q',new String[]{
                    "O_","9","(",")","0","kw"
            });
            put('r',new String[]{
                    "|2","12",".-","|^","l2","Я","®"
            });
            put('s',new String[]{
                    "5","$","§"
            });
            put('t',new String[]{
                    "7","+","7`","'|'","`|`","~|~","-|-","']['","т"
            });
            put('u',new String[]{
                    "|_|","\\_\\","/_/","\\_/","(_)","[_]","{_}"
            });
            put('v',new String[]{
                    "\\/"
            });
            put('w',new String[]{
                    "\\/\\/","(/\\)","\\^/","|/\\|","\\X/","\\'","'//","VV","\\_|_/","\\//\\//","Ш","2u","\\V/"
            });
            put('x',new String[]{
                    "%","*","><","}{",")(","Ж"
            });
            put('y',new String[]{
                    "`/","¥","\\|/","Ч","ү","у"
            });
            put('z',new String[]{
                    "2","5","7_",">_","(/)"
            });
            put('0',new String[]{
                    "O","D"
            });
            put('1',new String[]{
                    "I","L"
            });
            put('2',new String[]{
                    "Z"
            });
            put('3',new String[]{
                    "E"
            });
            put('4',new String[]{
                    "h"
            });
            put('5',new String[]{
                    "S"
            });
            put('6',new String[]{
                    "b","G"
            });
            put('7',new String[]{
                    "T","j"
            });
            put('8',new String[]{
                    "B"
            });
            put('9',new String[]{
                    "g","J"
            });
        }
    };

    /* 
        Ezt a metódust hívjuk meg, ha át akarunk alakítani egy stringet ilyen l33t text-re, paraméterként átadjuk a sima stringet
        majd visszakapjuk az átalakitottat
    */
    public static String convert(String plain)
    {
        String converted = ""; // ebbe fogjuk másolni az átalakított stringet
        for(int i=0;i<plain.length();i++)
            converted += getRandomCharacter(plain.toLowerCase().charAt(i)); // ebben egy private metódus segíthet, amely kiválasztja a random karaktert a tömbből

        return converted; // visszatérünk vele
    }
    private static String getRandomCharacter(char character)
    {
        String [] availableCharacters = characters.get(character); // az adott keyhez tartozó array lekérése

        if(availableCharacters == null) // ha az adott karakterre nincs leet karakter, akkor visszatérünk az eredetivel
            return ""+character+"";

        int x = (int)(Math.random() * ( availableCharacters.length)); // kiválasztunk egy random karaktert a tömb elemeiből

        return availableCharacters[x]; // majd visszatérünk a random karakterrel
    }
}