// Chomsky feladat megoldása: Deklaráció
#include <iostream>

int main()
{
    // A következö nevek bevezetée:

    // Egész:
    int a;
    // Egészre mutató mutató
    int *b = &a;
    // Egész referenciája
    int &r = a;
    // Egészek tömbje (5 elem)
    int c[5];
    // Egészek tömbjének referenciája (nem az első elemé)
    int (&tr)[5] = c;
    // Egészre mutató mutatók tömbje
    int *d[5];
    // Egészre mutató mutató visszaadó függvény
    int *h();
    // Egészre mutató mutatót vissaadó függvényre mutató mutató
    int *(*l)();
    // Egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
    int (*v (int c)) (int a,int b);
    // Függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
    int (*(*z) (int)) (int,int);

    // És még a feladathoz hozzátartozik, hogy sikerrel fordoljun le a program
    // Mivel c++ programról van szó, ezért a g++ deklaracio.cpp t használtuk

    return 0;
}