#include <stdio.h>

int main()
{
    // c89-ben nem volt hasznalhato az egysoros komment, mig c99 szabvanyban mar lehetett
    // ennek bemutatasara keszult el ez a video
    // ahogy latni lehet, hibat dob, ha ilyen kommenteket hasznalunk
    
    // Viszont c99-ben mar problema nelkul lefordul a program
    printf("Mukodik!\n");

    int n = 10;

    // Az alábbi kód hibát fog dobni, ha c89-el fordítjuk le, viszont c99 -el jó
    for (int i = 0; i<n;i++)
    {
        // feladat végrehajtása
    }

    // Az alábbi kód nem fog hibát dobni, ha c89-el fordítjuk le (c99-el is tökéletes értelemszerüen)
    int i;
    for (i = 0; i<n;i++)
    {
        // feladat végrehajtása
    }

    return 0;
}