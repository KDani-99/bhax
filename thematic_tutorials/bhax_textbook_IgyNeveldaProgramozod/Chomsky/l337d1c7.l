/* lex -o l337d1c7.c l337d1c7.l -> a lex c forrást készít az l filenkbol */
/* majd le kell fordítanunk a lex altal elkészített forrást a gcc -vel */
/* Majd lefuttatjuk a programot es kulönbözö inputokat adunk meg neki, amit at szeretnénk alakítani */
%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>
/* Használjuk a szükséges headereket */

  #define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher)) /* Ez a mérete a leet abc-nek, ami jelen esetben 36 karaktert tartalmaz, ez kiszámolja a méretét */
    
  struct cipher { /* Készítünk egy struktúrát, melyben a bemeneti karakterhez rendelni fogunk egy, a karakterhez tartozó 4 karakter közül az egyiket (pl. az a -hoz akár a 4 -et , vagy a @-ot) */
    char c;
    char *leet[4]; /* a 4 karakter, ami hozzátartozik */
  } l337d1c7 [] = { /* Itt tároljuk a karakterekhez tartozó karaktereket (méretét nem adtuk meg, ezt a fordító kiszámolja = 36 )*/
	/* A wikipedian vannak javaslatok, hogy milyen karaktert érdemes használnunk az egyes karakterekhez */

  {'a', {"4", "Д", "@", "/-\\"}},
  {'b', {"j3", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };
  
%}
%%
.	{ /* Itt a . (pont) azt jelenti, hogy barmilyen karaktert kap inputnak, atalakitja (pl a space-t nem fogja értelemszerüen )*/
	  
	  int found = 0;
	/* Végigmegyünk a karakterek tömbjén*/
	  for(int i=0; i<L337SIZE; ++i)
	  {
	  	/* Ha megtaláltuk a számunkra megfelelő karaktert (amivel megegyezik), akkor kisbetüssé alakítjuk a tolower() függvénnyel */
	    if(l337d1c7[i].c == tolower(*yytext))
	    {
	    /* Kiválasztunk random egy karaktert amivel kicseréljük a 4 közül (pl 9 -> g,g vagy j,j lehet )*/
	      int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
	    
          if(r<91)
	        printf("%s", l337d1c7[i].leet[0]);
          else if(r<95)
	        printf("%s", l337d1c7[i].leet[1]);
	      else if(r<98)
	        printf("%s", l337d1c7[i].leet[2]);
	      else 
	        printf("%s", l337d1c7[i].leet[3]);

	      found = 1;
	      break;
	    }
	    
	  }
	  
	  if(!found)
	     printf("%c", *yytext);	  
	  
	}
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}
