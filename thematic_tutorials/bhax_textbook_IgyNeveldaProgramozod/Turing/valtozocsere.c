
#include <stdio.h>

int main()
{
    
    int a = 57;
    int b = 13;

    printf("\nSegedvaltozos Csere -> \n\n");

    int temp = a;
    a = b;
    b = temp;

    printf("A -> %d\n",a);
    printf("B -> %d\n",b);

    printf("\n1. Csere -> \n\n");

    a += b;
    b = a - b;
    a = a - b;

    printf("A -> %d\n",a);
    printf("B -> %d\n",b);

    printf("\n2. Csere -> (XOR) \n\n");

    a = a ^ b;
    b = a ^ b;
    a = a ^ b;

    printf("A -> %d\n",a);
    printf("B -> %d\n",b);

    printf("\n3. Csere -> \n\n");

    a = a * b;
    b = a / b;
    a = a / b;

    printf("A -> %d\n",a);
    printf("B -> %d\n",b);

    return 0;
}