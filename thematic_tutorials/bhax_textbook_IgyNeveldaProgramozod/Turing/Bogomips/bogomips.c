#include <time.h>
#include <stdio.h>
// megfelelo headerek hasznalata

// keszitunk egy delay fuggvenyt, melynek elso parametere egy nemnegativ long tipusu valtozo (unsigned)
void delay(unsigned long long loops)
{
    for(unsigned long long i = 0;i < loops;i++);
}

int main()
{
    unsigned long long loops_per_sec = 1;
    unsigned long long ticks; 

    printf ("Calibrating delay loop..");
    fflush (stdout);

    while((loops_per_sec <<= 1))
    {
        // megnezzuk, hogy mennyi idot hasznalt a program
        ticks = clock();

        delay(loops_per_sec); // meghivjuk a delay fuggvenyunket, melynek a loop_per_sec-et adjuk parameteruk, ami a ketto hatvanyai lesz, 2-tol kezdodoen

        ticks = clock() - ticks;
        // kiszamoljuk, hogy mennyi ido telt el az elozo ido es a jelenlegi ido kozott

        if(ticks >= CLOCKS_PER_SEC) // a CLICKS_PER_SEC az egy konstans a time.h-bol, erteke 1.000.000 
        {
            loops_per_sec = (loops_per_sec / ticks) * CLOCKS_PER_SEC;
            // es vegul kiiratjuk az eredmenyt
            printf ("ok - %llu.%02llu BogoMIPS\n", loops_per_sec / 500000,
		  (loops_per_sec / 5000) % 100);

            return 0; // sikerrel hajtodott vegre a program
        }
    }
    // ha ide eljutunk, akkor valami hiba tortent es nem sikerult kiszamolnunk az erteket
    printf("Hiba\n");
    return -1;
}
