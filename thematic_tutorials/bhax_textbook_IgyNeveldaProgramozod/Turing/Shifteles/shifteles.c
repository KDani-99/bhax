#include <stdio.h>

int main()
{
    // felveszunk 2 int tipusu valtozot
    int a = 1;
    int b = 0;

    // ameddig a erteke nem 0, addig b erteket noveljuk 1 el
    while(a != 0)
    {
        a = a << 1; // left bitshift operator hasznalata
        b++;
    }
    // kiiratjuk az eredmenyt, ami 32 lesz, mivel 32 bites az int
    printf("%d\n",b);

    return 0;
}