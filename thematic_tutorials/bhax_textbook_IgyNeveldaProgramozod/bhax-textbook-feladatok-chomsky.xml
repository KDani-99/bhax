<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Decimálisból unárisba átváltó Turing gép</title>
        <para>
            Állapotátmenet gráfjával megadva írd meg ezt a gépet!
        </para>
         <para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="./Chomsky/TuringGep/turing.png" />
                </imageobject>
            </mediaobject>
        </para>
        <para>
            A gépről: (Forrás: Wikipedia)
        </para>
        <para>
            A Turing-gép fogalmát Alan Turing angol matematikus dolgozta ki 1936-ban. Matematikai számítási eljárások, algoritmusok precíz leírására, tágabb értelemben pedig mindenfajta „gépies” problémamegoldó folyamat, automatikusan végrehajtható számítás, például az akkoriban még nem létező számítógépek működésének modellezésére hozta létre.
        </para>
        <para>
            A gép minden állapotban beolvas egy betű és
            <programlisting language="c"><![CDATA[
            1. Az aktuális cellába beír egy meghatározott szimbólumot
            2. Az olvasófej a társzalagon balra vagy jobbra lép, esetleg helyben marad
            3. A gép átvált a jelenlegi állapotból egy másikra, ami akár lehet az, amibe volt, vagy egy speciális stop állapot (Megáll a gép)
            ]]></programlisting>
        </para>
        <para>
            Feladatoktól függően 2 kimenete lehet a gépnek:
            <programlisting language="c"><![CDATA[
            1. Szabályos megállás (terminálás): a gép leálló belső állapotba (stop-állapot) kerül
            2. Elszállás: a gép végtelen ideig fut.
            ]]></programlisting>
        </para>
        <para>
            A feladat az, hogy készítsünk egy olyan Turing gépet, amely, általunk leggyakrabban használt számrendszerből (10-es)-ből a legegyszerübb számrendszerbe vált át, az 1-es be. 
        </para>
        <para>
            Az egyes számrendszerben egy általunk megadott számot úgy ábrázolunk, hogy egy 1-es értéket jelölő szimbólumot annyiszor írunk le, amennyi az adott szám. Például:
            <programlisting language="c"><![CDATA[4 = IIII vagy 1111]]></programlisting>
        </para>
    </section>        
        
    <section>
        <title>Az a<superscript>n</superscript>b<superscript>n</superscript>c<superscript>n</superscript> nyelv nem környezetfüggetlen</title>
        <para>
            Mutass be legalább két környezetfüggő generatív grammatikát, amely ezt a nyelvet generálja!
        </para>
        <para>
            Chomsky célja ezzel az volt, hogy a természetes nyelveket szerette volna generatív nyelvtannal leírni.
        </para>
        <para>
            Környezetfüggő példa:
            <programlisting language="c"><![CDATA[
            S, X, Y - ezek változók
            a, b, c - ezek konstansok
            S -> abc, S -> aXbc, Xb -> bX, Xc -> Ybcc, bY -> Yb, aY -> aaX, aY -> aa
            S-ből indulunk ki
            
            S (S -> aXbc)
            aXbc (Xb -> bX)
            abXc (Xc -> Ybcc)
            abYbcc (bY -> Yb)
            aYbbcc (aY -> aa)
            aabbcc
            ]]></programlisting>
            Környezetfüggő másik példa: (hossznemcsökkentő)
            <programlisting language="c"><![CDATA[
            P1XP2 -> P1QP2, P1, P2 eleme (VN U VT)*, X VN beli, Q (VN U VT)+ beli, kivéve S -> üres, de akkor S nem lehet jobb oldali egyetlen szabályban sem
            ]]></programlisting>
        </para>
    </section>        
                
    <section>
        <title>Hivatkozási nyelv</title>
        <para>
            A <citation>KERNIGHANRITCHIE</citation> könyv C referencia-kézikönyv/Utasítások melléklete alapján definiáld 
            BNF-ben a C utasítás fogalmát!
            Majd mutass be olyan kódcsipeteket, amelyek adott szabvánnyal nem fordulnak (például C89), mással (például C99) igen.
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtube.com/watch?v=Z37zY4YKH3A">https://youtube.com/watch?v=Z37zY4YKH3A</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/c89.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/c89.c</filename>
            </link>  
        </para>
        <para>
         Az utasítások olyan sorrendben hajtódnak végre, amilyenben leírjuk őket. Lehetőségünk van jelezni, hogy ha más sorrendben akarjuk őket végrehajtani.
         <programlisting language="c"><![CDATA[
            Utasításcsoportok: 
            
                <utasítás>::=
                    <összetett_utasítás>
                    <kifejezésutasítás> 
                    <címkézett_utasítás>
                    <kiválasztó_utasítás>
                    <iterációs_utasítás>
                    <vezérlésátadó_utasítás>

            Címkézett utasítások szintaktikája:
                
                <címkézett_utasítás>::=
                    azonosító_utasítás
                    case állandó_kifejezés : utasítás
                    default: utasítás

            Kifejezésutasítások:

                <kifejezésutasítás>::=
                    kifejezés_opció

            Összetett utasítás:

                <összetett_utasítás>::=
                    {deklarációs_lista utasítás_lista}
                <deklarációs_lista>::=
                    deklaráció
                    delarációs_lista deklaráció
                <utasítás_lista>::=
                    utasítás
                    utasítás_lista utasítás

            Kivalsztó utasítások:

                <kiválasztó_utasítás>::=
                    if (kifejezés) utasítás
                    if (kifejezés) utasítás else utasítás
                    switch (kifejezés) utasítás
            
            Iterációs utasítások:

                <iterációs_utasítás>::=
                    while (kifejezés) utasítás
                    do utasítás while (kifejezés)
                    for (kifejezés;kifejezés;kifejezés) utasítás

            Vezérlésátadó utasítások:

                <vezérlésátadó_utasítás>::=
                    goto azonosító
                    continue
                    break
                    return kifejezés
            
            ]]></programlisting>
        </para>
        <para>
            Az alábbi szintaxist, ahol az i változót a ciklusban deklaráljuk csak a C99-es szabványban vezették be. (gcc forras.c -o outfile -std=c99)
            <programlisting language="c"><![CDATA[for (int i = 0; i<n;i++) ]]></programlisting>
            Mielőtt bevezették, addig a ciklus előtt kellett deklarálni a ciklusban használt változót az alábbi módon: (C89 -ben)
            <programlisting language="c"><![CDATA[
            int i;
            for (i = 0; i<n;i++)
            {
                // feladat végrehajtása
            }
            ]]></programlisting>
        </para>
        <para>
            Egy másik ilyen szabvány különbség az, hogy C89 -ben nem lehetett egysoros kommentet írni
            <programlisting language="c"><![CDATA[// komment]]></programlisting>
            -t használva, csak a
            <programlisting language="c"><![CDATA[/* komment */]]></programlisting>
            volt használható, ám a C99-ben lehet. (Ansi C)
        </para>
        <programlisting language="c"><![CDATA[
            #include <stdio.h>

            int main()
            {
                // c89-ben nem volt hasznalhato az egysoros komment, mig c99 szabvanyban mar lehetett
                // ennek bemutatasara keszult el ez a video
                // ahogy latni lehet, hibat dob, ha ilyen kommenteket hasznalunk
                
                // Viszont c99-ben mar problema nelkul lefordul a program
                printf("Mukodik!\n");

                int n = 10;

                // Az alábbi kód hibát fog dobni, ha c89-el fordítjuk le, viszont c99 -el jó
                for (int i = 0; i<n;i++)
                {
                    // feladat végrehajtása
                }

                // Az alábbi kód nem fog hibát dobni, ha c89-el fordítjuk le (c99-el is tökéletes értelemszerüen)
                int i;
                for (i = 0; i<n;i++)
                {
                    // feladat végrehajtása
                }

                return 0;
            }
        ]]></programlisting>
    </section>                     

    <section>
        <title>Saját lexikális elemző</title>
        <para>
            Írj olyan programot, ami számolja a bemenetén megjelenő valós számokat! 
            Nem elfogadható olyan megoldás, amely maga olvassa betűnként a bemenetet, 
            a feladat lényege, hogy lexert használjunk, azaz óriások vállán álljunk és ne kispályázzunk!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/9KnMqrkj_kU">https://youtu.be/9KnMqrkj_kU</link> (15:01-től).
            <link xlink:href="https://youtu.be/fzTSmsZhdOU">https://youtu.be/fzTSmsZhdOU</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/realnumber.l">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/realnumber.l</filename>
            </link> 
        </para>
        <para>
        A program 3 részből áll, melyeket a %% választ el egymástól.
        <programlisting language="c"><![CDATA[%{
        #include <stdio.h>
        int realnumbers = 0;
        %}
        digit	[0-9]
        %%
        {digit}*(\.{digit}+)?	{++realnumbers; 
            printf("[realnum=%s %f]", yytext, atof(yytext));}
        %%
        int
        main ()
        {
        yylex ();
        printf("The number of real numbers is %d\n", realnumbers);
        return 0;
        }
        ]]></programlisting>
        </para>
        <para>
            A lexer C forrást készít a bemenetből.
            Használata:
            <programlisting language="c"><![CDATA[lex -o <outputfile>.c <inputfile>.l]]></programlisting>
        </para>
        <para>
        Ennek a programnak az a feladata, hogy felismerje a valós számokat a bemenetből. Ezekhez írtunk egy lexikális elemzőt. A felismerés tokenek alapján történik, mely az alábbi rész tesz lehetővé:
        A program a standard inputrol veszi az adatot (kifejezéseket), majd tokenekké alakítja, és így ismeri fel a számokat (a mintánknak megfelelően).
        <programlisting language="c"><![CDATA[
        digit	[0-9] // számjegyek 0-tol 9-ig, ezeket akarjuk felismerni
        %%
        {digit}*(\.{digit}+)? 
        // a '*' előtt akármennyi szám lehet (számjegy), akár 0 darab is. 
        // a '\.' előtt nem muszáj számnak lennie, hiszen akár a .1 et is képes felismerni (kezdődhet .-al)
        // '+' azt jelenti, ha a pont talalható a kifejezésben, ahhoz, hogy felismerje számként, legalább 1 számjegynek lennie kell a . után, hogy akárhány darab lehet a számjegyből, de legalább 1 -nek kell lennie, hogy felismerje
        // ezen kifejezés segítségével készítünk tokeneket
        // az atof() függvény átkonvertálja a stringet -> doublera
        %%
        ]]></programlisting>
        Használata pedig a szokásos main függvényünkben.
        <programlisting language="c"><![CDATA[
        int
        main ()
        {
            yylex (); // meghívjuk a függvényt
            printf("The number of real numbers is %d\n", realnumbers); // kiíratjuk az eredményt
            return 0;
        }
        ]]></programlisting>
        Egy pár bemenettel kipróbálva:
        </para>
        <mediaobject>
                <imageobject>
            <imagedata fileref="./Chomsky/lexer/lexer.png" scale="50" />
            </imageobject>
        </mediaobject>
    </section>                     

    <section>
        <title>Leetspeak</title>
        <para>
            Lexelj össze egy l33t ciphert!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/44pywOuizfw">https://youtu.be/44pywOuizfw</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/l337d1c7.l">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/l337d1c7.l</filename>
            </link>  
        </para>
        <para>
        A kód elemzése:
        </para>
        <para>
        <programlisting language="c"><![CDATA[%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>
/* Használjuk a szükséges headereket */

  #define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher)) /* Ez a mérete a leet abc-nek, ami jelen esetben 36 karaktert tartalmaz, ez kiszámolja a méretét */
    
  struct cipher { /* Készítünk egy struktúrát, melyben a bemeneti karakterhez rendelni fogunk egy, a karakterhez tartozó 4 karakter közül az egyiket (pl. az a -hoz akár a 4 -et , vagy a @-ot) */
    char c;
    char *leet[4]; /* a 4 karakter, ami hozzátartozik */
  } l337d1c7 [] = { /* Itt tároljuk a karakterekhez tartozó karaktereket (méretét nem adtuk meg, ezt a fordító kiszámolja = 36 )*/
	/* A wikipedian vannak javaslatok, hogy milyen karaktert érdemes használnunk az egyes karakterekhez */

  {'a', {"4", "/-\\", "@", "a"}},
  {'b', {"j3", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };
  
%}
%%
.	{ /* Itt a . (pont) azt jelenti, hogy barmilyen karaktert kap inputnak, atalakitja (pl a space-t nem fogja értelemszerüen )*/
	  
	  int found = 0;
	/* Végigmegyünk a karakterek tömbjén*/
	  for(int i=0; i<L337SIZE; ++i)
	  {
	  	/* Ha megtaláltuk a számunkra megfelelő karaktert (amivel megegyezik), akkor kisbetüssé alakítjuk a tolower() függvénnyel */
	    if(l337d1c7[i].c == tolower(*yytext))
	    {
	    /* Kiválasztunk random egy karaktert amivel kicseréljük a 4 közül (pl 9 -> g,g vagy j,j lehet )*/
	      int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
	    
          if(r<91)
	        printf("%s", l337d1c7[i].leet[0]);
          else if(r<95)
	        printf("%s", l337d1c7[i].leet[1]);
	      else if(r<98)
	        printf("%s", l337d1c7[i].leet[2]);
	      else 
	        printf("%s", l337d1c7[i].leet[3]);

	      found = 1;
	      break;
	    }
	    
	  }
	  
	  if(!found)
	     printf("%c", *yytext);	  
	  
	}
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}
}]]></programlisting>
</para>
        <para>
            A leet az egy alternatív, internetes ABC-je az angol nyelvnek. Külömböző ASCII karaktereket használva cseréljük ki a szövegekből a betüket, melyek hasonlítanak az eredetire (Pl: L33t = Leet). Főként interneten használják ezt a nyelvet fórumokon, vagy esetleg játékokban.
        </para>
        <para>
            A program használata:
            <programlisting language="c"><![CDATA[lex -o l337d1c7.c l337d1c7.l]]></programlisting>
            A lex készíti el a C forrást, melyet még le kell fordítanunk, melyhez szükséges a flex library (-lfl):
            <programlisting language="c"><![CDATA[gcc l337d1c7.c -o l337d1c7 -lfl]]></programlisting>
            Az általunk beírt bemenetből készít ilyen l33t szöveget. Például az alábbiak:
            <programlisting language="c"><![CDATA[
            Chomsky feladat megoldasa

            -> c4[]msky f3l4d4t m3g0|d4s4
            ]]></programlisting>
        </para>
    </section>                     


    <section>
        <title>A források olvasása</title>
        <para>
            Hogyan olvasod, hogyan értelmezed természetes nyelven az alábbi kódcsipeteket? Például
            <programlisting><![CDATA[if(signal(SIGINT, jelkezelo)==SIG_IGN)
    signal(SIGINT, SIG_IGN);]]></programlisting>
            Ha a SIGINT jel kezelése figyelmen kívül volt hagyva, akkor ezen túl is legyen
            figyelmen kívül hagyva, ha nem volt figyelmen kívül hagyva, akkor a jelkezelo függvény
            kezelje. (Miután a <command>man 7 signal</command> lapon megismertem a SIGINT jelet, a
            <command>man 2 signal</command> lapon pedig a használt rendszerhívást.)
        </para>

        <caution>
            <title>Bugok</title>
            <para>
                Vigyázz, sok csipet kerülendő, mert bugokat visz a kódba! Melyek ezek és miért? 
                Ha nem megy ránézésre, elkapja valamelyiket esetleg a splint vagy a frama?
            </para>
        </caution>
            
        <orderedlist numeration="lowerroman">
            <listitem>                                    
                <programlisting><![CDATA[if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
    signal(SIGINT, jelkezelo);]]></programlisting>
            </listitem>
            <listitem>                       
                <programlisting><![CDATA[for(i=0; i<5; ++i)]]></programlisting>    
                <para>
                Egy sima for ciklus, mely 5x fog lefutni, i első értéke 0, utolsó értéke 4 lesz. A ++i azt jelenti, hogy pre-incremental, azaz i visszatérne az új, növelt értékével .
                </para>         
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[for(i=0; i<5; i++)]]></programlisting>    
                 <para>
                Ez szintén egy sima for ciklus, mely ugyan úgy mint az előző, 5x fog lefutni, viszont itt i++ szerepel, ami azt jelenti, hogy post-incremental, azaz i először visszatérni a régi értékkel, majd utánna növelné, így csak a kövektkező használatnál kapnánk meg az új értékét. Mindkét esetben i értéke azonos lesz, minden lépésben.
                </para>           
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[for(i=0; i<5; tomb[i] = i++)]]></programlisting>       
                <para>
                Ez a kódrészlet hibás, a Splint felismeri. Az alábbi hibaüzenetet dobja:
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Chomsky/Forrasok/hiba1.png" />
                    </imageobject>
                </mediaobject>
                A kiértékelés sorrendje nincs definiálva.
                </para> 
                <para>   
                Amennyiben nincs legalább 5 elem a tömbben, az alábbi hiba léphet fel fel:
                <mediaobject>
                    <imageobject>
                        <imagedata fileref="./Chomsky/Forrasok/forrasok.png" />
                    </imageobject>
                </mediaobject>
                És a kód, ami előidézte:
                <programlisting><![CDATA[
                #include <stdio.h>
                #include <stdlib.h>

                int main()
                {
                    int tomb[3];
                    // Ez a kód amúgy is hibás, főleg ha a tömb kisebb méretü, mint ahányszor végrehajtódik a ciklus
                    for(int i=0; i<5; tomb[i] = ++i)
                        printf("%d\n",tomb[i]);
                    return 0;
                }
                ]]></programlisting>
                </para>  
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[for(i=0; i<n && (*d++ = *s++); ++i)]]></programlisting>         
                <para>     
                    Itt azt szerettük volna elérni, ha két feltétel alapján müködne a ciklusunk, ám ahhoz, hogy igazságérték legyen a jobb oldali feltételből, a == operátort kellett volna alkalmaznunk. Az and (és) operátor miatt mind a két feltételnek teljesülni kell, hogy végrehajtódjon a ciklus. 
                </para>  
                <para>   
                    A Splint itt is segítségünkre lehet, ugyanis szintén jelzi, hogy valami hiba van:
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="./Chomsky/Forrasok/hiba2.png" />
                        </imageobject>          
                    </mediaobject>
                Írja is a Splint, hogy igazságértéket vár az és jobb oldalán, de nem azt talált, hanem értékadás operátort.
                </para>
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[printf("%d %d", f(a, ++a), f(++a, a));]]></programlisting>  
                <para>   
                    Ebben a kódban is van hiba, hiszen a kiértékelés sorrendje nem definiált a függvény paramétereinél. Ezt a hibát is megtalálja a Splint és jelzi.
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="./Chomsky/Forrasok/hiba3.png" />
                        </imageobject>          
                    </mediaobject>
                </para>         
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[printf("%d %d", f(a), a);]]></programlisting>     
                <para>   
                    Itt nem található hiba.
                    <mediaobject>
                        <imageobject>
                            <imagedata fileref="./Chomsky/Forrasok/hibamentes.png" />
                        </imageobject>          
                    </mediaobject>
                </para>       
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[printf("%d %d", f(&a), a);]]></programlisting>         
                <para>   
                    Itt sem található hiba.
                </para>   
            </listitem>
        </orderedlist>
        <para>
            Megoldás forrása:  
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            A különböző programbeli hibák felismerését segítheti a Splint, Farma, vagy memória szivárgást (memory leaket) esetleg a Valgrind.
        </para>
        <para>
            A hibák elkerülése a programokban nagyon fontos, mivel kisebb hibák is okozhatják a program nem megfelelő müködését, esetleg kifagyását.
        </para>
        <para>
            Az előző kódcsipetek tartalmaznak hibákat, melyeket (jelen esetben Splintet használva) a hibafelismerő felismer és egy üzenettel jelzi nekünk.
        </para>
        <para>
            Egy ilyen üzenetre példa:
            <mediaobject>
                <imageobject>
                    <imagedata fileref="./Chomsky/Forrasok/hiba1.png" />
                </imageobject>          
            </mediaobject>
        </para>
    </section>                     

    <section>
        <title>Logikus</title>
        <para>
            Hogyan olvasod természetes nyelven az alábbi Ar nyelvű formulákat?
        </para>
        <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})))$ 

$(\forall x \exists y ((x<y)\wedge(y \text{ prím})\wedge(SSy \text{ prím})))$ 

$(\exists y \forall x (x \text{ prím}) \supset (x<y)) $ 

$(\exists y \forall x (y<x) \supset \neg (x \text{ prím}))$
]]></programlisting>        
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX</link>
            <link xlink:href="Chomsky/logikus.tex">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/logikus.tex</filename>
            </link>
        </para>

        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/ZexiPy3ZxsA">https://youtu.be/ZexiPy3ZxsA</link>, <link xlink:href="https://youtu.be/AJSXOQFF_wk">https://youtu.be/AJSXOQFF_wk</link>
            <link xlink:href="https://youtu.be/rQBfSCItods">https://youtu.be/rQBfSCItods</link>
        </para>

        <para>
            A Latex matematikában kvantorok a forall és exists szimbólumok. A forall az univerzális kvantor (minden), míg az exists az egzisztenciális kvantor (létezik). A felső példában még használunk pár szimbólumot, mint például logikai operátorokat, mint az implikáció (supset).
        </para>
        <para>
            Az alábbi formula: Minden X -re létezik olyan Y, ami nagyobb X-nél és prím
             <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})))$ ]]></programlisting>   
        </para>
        <para>
            Az alábbi formula: Minden X -re létezik olyan Y, ami nagyobb X-nél és prím és az Y (S = 1) -nál kettővel nagyobb szám is prím
             <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})\wedge(SSy \text{ prím})))$ ]]></programlisting>   
        </para>
        <para>
            Az alábbi formula: Létezik olyan Y minden X-re, ahol X prím, tehát X a legnagyobb prím.
             <programlisting language="tex"><![CDATA[$(\exists y \forall x (x \text{ prím}) \supset (x<y)) $ ]]></programlisting>   
        </para>
        <para>
            Az alábbi formula: Létezik olyan Y minden X -re, ami kisebb mint X és X az nem prím.
             <programlisting language="tex"><![CDATA[$(\exists y \forall x (y<x) \supset \neg (x \text{ prím}))$]]></programlisting>   
        </para>
        <para>
        <mediaobject>
            <imageobject>
                <imagedata fileref="./Chomsky/logikus1.png" />
            </imageobject>
        </mediaobject>
        <mediaobject>
            <imageobject>
                <imagedata fileref="./Chomsky/logikus2.png" />
            </imageobject>
        </mediaobject>
        </para>
    </section>                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    <section>
        <title>Deklaráció</title>
            
        <para>
            Vezesd be egy programba (forduljon le) a következőket: 
        </para>

        <itemizedlist>
            <listitem>
                <para>egész</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egész referenciája</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbjének referenciája (nem az első elemé)</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatók tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvény</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvényre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény</para>                        
            </listitem>            
            <listitem>
                <para>függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre</para>                        
            </listitem>            
        </itemizedlist>            

        <para>
            Mit vezetnek be a programba a következő nevek?
        </para>

        <itemizedlist>
            <listitem>
                <programlisting><![CDATA[int a;]]></programlisting>   
                <para>
                    egész
                </para>         
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *b = &a;]]></programlisting>          
                <para>
                    egészre mutató mutató
                </para>   
            </listitem>
            <listitem>
                <programlisting><![CDATA[int &r = a;]]></programlisting>       
                <para>
                    egész referenciája
                </para>        
            </listitem>
            <listitem>
                <programlisting><![CDATA[int c[5];]]></programlisting>        
                <para>
                    egészek tömbje (5 elem)
                </para>       
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (&tr)[5] = c;]]></programlisting>      
                <para>
                    egészek tömbjének referenciája (nem az első elemé)
                </para>         
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *d[5];]]></programlisting>           
                <para>
                    egészre mutató mutatók tömbje
                </para>    
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *h ();]]></programlisting>   
                <para>
                    egészre mutató mutatót visszaadó függvény
                </para>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *(*l) ();]]></programlisting>  
                <para>
                    egészre mutató mutatót visszaadó függvényre mutató mutató     
                </para>        
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (*v (int c)) (int a, int b)]]></programlisting>                 
                <para>
                    egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény      
                </para>      
            </listitem>            
            <listitem>
                <programlisting><![CDATA[int (*(*z) (int)) (int, int);]]></programlisting>    
                <para>
                    függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
                </para>          
            </listitem>            
        </itemizedlist>       


        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/qKpJRDVuqzk">https://youtu.be/qKpJRDVuqzk</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Chomsky/deklaracio.cpp">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/deklaracio.cpp</filename>
            </link> 
        </para>
        <para>
            Az utolsó két deklarációs példa demonstrálására két olyan kódot
            írtunk, amelyek összahasonlítása azt mutatja meg, hogy miért 
            érdemes a <command>typedef</command> használata: <link xlink:href="Chomsky/fptr.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr.c</filename>
            </link>,
            <link xlink:href="Chomsky/fptr2.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr2.c</filename>.
            </link>  
            
        </para>
        <programlisting><![CDATA[#include <stdio.h>

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

int (*sumormul (int c)) (int a, int b)
{
    if (c)
        return mul;
    else
        return sum;

}

int
main ()
{

    int (*f) (int, int);

    f = sum;

    printf ("%d\n", f (2, 3));

    int (*(*g) (int)) (int, int);

    g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}]]></programlisting>            
        <programlisting><![CDATA[#include <stdio.h>

typedef int (*F) (int, int);
typedef int (*(*G) (int)) (int, int);

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

F sumormul (int c)
{
    if (c)
        return mul;
    else
        return sum;
}

int
main ()
{

    F f = sum;

    printf ("%d\n", f (2, 3));

    G g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}
]]></programlisting>      
        <para>  
            A typedef célja, hogy már létező típusokhoz rendelhetünk egy másik, alternatív nevet.
        </para>    
        <para>
            A typedef egyik előnye, hogy sokkal érthetőbbé és egyszerübbé teszi a kódunkat, akár más fejlesztők számára is. Lehetővé teszi a bonyolultabb objektumok egyszerübb elnevezését, akár 1 karakterből álló névre, melyet gyorsan és könnyen tudunk használni.
        </para>
        <para>
            Példa:
            <programlisting><![CDATA[
            typedef int A;
            int main()
            {
                A egesz = 99;
                printf("%d\n",egesz); // -> 99 
                return 0;
            }
            ]]></programlisting>    
        </para>
    </section>                     

    <section>
        <title>Vörös Pipacs Pokol - Csiga diszkrét</title>
        <para>
            A második Minecraft Malmo feladat az volt, hogy az XML ben a mozgás a ContinousMovementCommand-ról módosítani kellett DiscreteMovementCommand-ra az AgentHandlerhez. A DiscreteMovementCommand-ban Steve másként mozog, tulajdonképpen csak 'teleportálgat' (nyilván nem egy TP parancshoz hasonlóan).
        </para>
        <para>
            A parancsok szinte ugyan azok, pár kivételével. 
            A mozgás maradt a move, az ugrás a jump, viszont így jóval egyszerübb és gyorsabb forogni, illetve mozogni.
        </para>
        <para>
            Megoldás videó:  <link xlink:href="https://youtu.be/XjWLzH4KJnM">https://youtu.be/XjWLzH4KJnM</link>
        </para>
        <para>
            (Mivel a versenyben is csiga mozgás volt, DiscreteMovementCommand-al, így az alábbi forrás is megfelel ide)
            Megoldás forrása: <link xlink:href="https://github.com/KDani-99/RFLAgent/tree/master/sense.py">https://github.com/KDani-99/RFLAgent/tree/master/sense.py</link>
        </para>
        <para>
            <programlisting><![CDATA[
            def run(self):
 
         self.agent_host.sendCommand("turn 0")
         self.agent_host.sendCommand( "move 1" )
        time.sleep(0.5)
 
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
 
        turnCount = 0
       
        while world_state.is_mission_running:
 
            if world_state.number_of_observations_since_last_state > 0:
                msg = world_state.observations[-1].text
                data = json.loads(msg)
                print(data)
                self.agent_host.sendCommand("move 1")
                if "nbr3x3" in data:
                    blocks = data["nbr3x3"]
                    print(data)
                    if len(blocks) >= 16:
                        #turnCount = direction
                        # ha eleri az adott iranyban a blokkot es az fold, akkor megfordul, igy minden oldalon vegigmegy
                        if (turnCount == 1 and blocks[12] == "dirt") or (turnCount == 2 and blocks[10] == "dirt") or (turnCount == 3 and blocks[14] == "dirt") or (turnCount == 0 and blocks[16] == "dirt"):
                            if turnCount == 3:
                                self.jump(turnCount)
                            self.turn(turnCount)
                            turnCount += 1
                            if turnCount == 4:
                                turnCount = 0
 
 
            world_state = self.agent_host.getWorldState()
            ]]></programlisting>    
        </para>
    </section>
    <section>
        <title>Red Flower Hell - 2. C++</title>
         <para>
           A Minecraft Malmo második szakkörének C++ -ban történő megoldása.
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/KDani-99/RFLAgent/blob/master/rfii.cpp">https://github.com/KDani-99/RFLAgent/blob/master/rfii.cpp</link>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/_6jeTJ8_-Ak">https://youtu.be/_6jeTJ8_-Ak</link>
        </para>
    </section>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
